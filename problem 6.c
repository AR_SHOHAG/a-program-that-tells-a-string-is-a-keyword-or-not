#include<stdio.h>
#include<string.h>

int check(char input[])
{
    int i, temp = 0;
    char key[32][20] = {"auto", "break", "case", "char", "const", "continue", "default", "do", "double", "else", "enum", "extern", "float", "for", "goto", "if", "int", "long", "register", "return", "short", "signed", "sizeof", "static", "struct", "switch", "tyepof", "union", "unsigned", "void", "volatile", "while"};

    for(i=0;i<32; ++i){
        if(strcmp(key[i], input))
            temp++;
    }
    return 32-temp;
}

int main()
{
    char inp[20];

    printf("Enter a word\n");

    gets(inp);

    if(check(inp))
        printf("%s is a Keyword\n", inp);
    else
        printf("%s is not a Keyword\n", inp);

    return 0;
}
